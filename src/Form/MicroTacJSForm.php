<?php

namespace Drupal\micro_tacjs\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\micro_site\Entity\Site;
use Drupal\micro_site\Entity\SiteInterface;
use Drupal\tacjs\Form\Steps\ManageDialog;

/**
 * Class TacJSForm.
 */
class MicroTacJSForm extends ManageDialog {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'micro_tacjs_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['micro_tacjs.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, SiteInterface $site = NULL) {
    if (!$site instanceof SiteInterface) {
      $form = [
        '#type' => 'markup',
        '#markup' => $this->t('TacJS settings is only available in a micro site context.'),
      ];
      return $form;
    }
    $form = parent::buildForm($form, $form_state);

    $form['site_id'] = [
      '#type' => 'value',
      '#value' => $site->id(),
    ];

    foreach ($form as $key => &$field) {
      if (!empty($site->getData('micro_tacjs')) && !empty($site->getData('micro_tacjs')[$key])) {
        $field['#default_value'] = $site->getData('micro_tacjs')[$key];
      }
    }

    $form['tacjs_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#description' => $this->t('Check this option to enabled the TacJS. Uncheck to disable the TacJS.'),
      '#default_value' => (bool) !empty($site->getData('micro_tacjs')),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $site_id = $form_state->getValue('site_id');
    $site = Site::load($site_id);
    if (!$site instanceof SiteInterface) {
      $form_state->setError($form, $this->t('An error occurs. Impossible to find the site entity.'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $site_id = $form_state->getValue('site_id');
    $site = Site::load($site_id);
    if (!$site instanceof SiteInterface) {
      return;
    }
    $langcode = $this->getCurrentLanguageId();
    $data = [];

    if (empty($form_state->getValue('tacjs_enabled'))) {
      $site->setData('micro_tacjs', $data);
      $site->save();
    }
    else {
      $values = $form_state->getValues();
      // Save config.
      foreach ($values as $key => $value) {
        if (isset($value) && !in_array($key, [
          'form_id',
          'form_build_id',
          'form_token',
          'op',
          'submit',
        ])) {
          $data[$key] = $value;
        }
      }
      $data['langcode'] = $langcode;
      $site->setData('micro_tacjs', $data);
      $site->save();
    }
  }

  /**
   * Get current language.
   */
  protected function getCurrentLanguageId() {
    return \Drupal::languageManager()->getCurrentLanguage()->getId();
  }

}
