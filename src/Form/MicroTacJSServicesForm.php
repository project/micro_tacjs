<?php

namespace Drupal\micro_tacjs\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\micro_site\Entity\Site;
use Drupal\micro_site\Entity\SiteInterface;
use Drupal\tacjs\Form\Steps\AddServices;

/**
 * Class MicroTacJSServicesForm.
 *
 * Define service to activate.
 */
class MicroTacJSServicesForm extends AddServices {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'micro_tacjs_services_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['micro_tacjs_services.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, SiteInterface $site = NULL) {
    if (!$site instanceof SiteInterface) {
      $form = [
        '#type' => 'markup',
        '#markup' => $this->t('TacJS settings is only available in a micro site context.'),
      ];
      return $form;
    }
    $form = parent::buildForm($form, $form_state);

    $form['site_id'] = [
      '#type' => 'value',
      '#value' => $site->id(),
    ];

    foreach ($form['service'] as &$field) {
      foreach ($field as $subkey => &$subfield) {
        if (!empty($site->getData('micro_tacjs_services')) && is_array($subfield)) {
          $subfield['#default_value'] = $site->getData('micro_tacjs_services')[$subkey];
        }
        foreach ($subfield as $subkeydetails => &$subdetailsfield) {
          if (!empty($site->getData('micro_tacjs_services')[$subkeydetails]) && is_array($subdetailsfield)) {
            $subdetailsfield['#default_value'] = $site->getData('micro_tacjs_services')[$subkeydetails];
          }
        }
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $site_id = $form_state->getValue('site_id');
    $site = Site::load($site_id);
    if (!$site instanceof SiteInterface) {
      $form_state->setError($form, $this->t('An error occurs. Impossible to find the site entity.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $site_id = $form_state->getValue('site_id');
    $site = Site::load($site_id);
    if (!$site instanceof SiteInterface) {
      return;
    }
    $langcode = $this->getCurrentLanguageId();
    $data = [];

    if ((bool) empty($site->getData('micro_tacjs'))) {
      $site->setData('micro_tacjs_services', $data);
      $site->save();
    }
    else {
      $values = $form_state->getValues();
      // Save config.
      foreach ($values as $key => $value) {
        if (isset($value) && !in_array($key, [
          'form_id',
          'form_build_id',
          'form_token',
          'op',
          'submit',
          'services__active_tab',
        ])) {
          $data[$key] = $value;
        }
      }
      $data['langcode'] = $langcode;
      $site->setData('micro_tacjs_services', $data);
      $site->save();
    }
  }

  /**
   * Get current language.
   */
  protected function getCurrentLanguageId() {
    return \Drupal::languageManager()->getCurrentLanguage()->getId();
  }

}
