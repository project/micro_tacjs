<?php

namespace Drupal\micro_tacjs;

use Drupal\micro_site\Entity\SiteInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryOverrideInterface;
use Drupal\Core\Config\StorageInterface;

/**
 * Override TacJS configuration per micro site.
 *
 * @package Drupal\micro_site
 */
class MicroTacJSConfigOverrides implements ConfigFactoryOverrideInterface {

  /**
   * The active micro site or NULL.
   *
   * @var \Drupal\micro_site\Entity\SiteInterface|null
   */
  protected $activeSite = NULL;

  /**
   * {@inheritdoc}
   */
  public function loadOverrides($names) {
    $overrides = [];

    if (in_array('tacjs.settings', $names)) {
      $site = $this->getActiveSite();
      if ($site instanceof SiteInterface) {
        $data_tacjs = $site->getData('micro_tacjs');
        $data_services = $site->getData('micro_tacjs_services');
        $services = $users = [];
        foreach ($data_services as $key => $value) {
          $settings = explode('_', $key, 3);
          if (isset($settings[1])) {
            $service = $settings[1];
            if (isset($settings[0]) && $settings[0] == 'service' && isset($settings[1])) {
              if (isset($data_services['service_' . $service . '_status'], $data_services['service_' . $service . '_needConsent'], $data_services['service_' . $service . '_readMore'], $data_services['service_' . $service . '_readmoreLink'])) {
                $services['services'][$service] = [
                  "status" => $data_services['service_' . $service . '_status'],
                  "needConsent" => $data_services['service_' . $service . '_needConsent'],
                  "readMore" => $data_services['service_' . $service . '_readMore'],
                  "readmoreLink" => $data_services['service_' . $service . '_readmoreLink'],
                ];
              }
            }
            if (isset($settings[0]) && $settings[0] == 'user' && isset($settings[1]) && $data_services['service_' . $service . '_status']) {
              $user_key = $settings[2];
              if (isset($data_services['user_' . $service . '_' . $user_key]) && !empty($data_services['user_' . $service . '_' . $user_key])) {
                $users['user'][$user_key] = $data_services['user_' . $service . '_' . $user_key];
              }
            }
          }
        }
        if ($data_tacjs && $data_services) {
          $overrides['tacjs.settings'] = ['dialog' => $data_tacjs] + $services + $users;
        }
      }
    }
    
    return $overrides;
  }

  /**
   * Get active site.
   */
  protected function getActiveSite() {
    // I don't use here a dependency injection because of a
    // CircularReferenceException thrown when injecting the negotiator.
    if (is_null($this->activeSite)) {
      /** @var \Drupal\micro_site\SiteNegotiatorInterface $negotiator */
      $negotiator = \Drupal::service('micro_site.negotiator');
      /** @var \Drupal\micro_site\Entity\SiteInterface $site */
      $this->activeSite = $negotiator->getActiveSite();
    }
    return $this->activeSite;

  }

  /**
   * {@inheritdoc}
   */
  public function getCacheSuffix() {
    return 'MicroTacJSConfigurationOverrider';
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata($name) {
    $meta = new CacheableMetadata();
    return $meta;
  }

  /**
   * {@inheritdoc}
   */
  public function createConfigObject($name, $collection = StorageInterface::DEFAULT_COLLECTION) {
    return NULL;
  }

}
